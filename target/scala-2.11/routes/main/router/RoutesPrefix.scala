
// @GENERATOR:play-routes-compiler
// @SOURCE:/home/vagrant/play-experiment/example-app/conf/routes
// @DATE:Sat Jan 16 20:27:44 UTC 2016


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
